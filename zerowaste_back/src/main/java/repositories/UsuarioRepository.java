package repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import entities.Fundacion;
import entities.Localidad;
import entities.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
	
	public Usuario findById(int id);
	
	public List<Usuario> findByTipoUsuario(String tipoUsuario);
	
	public List<Usuario> findByEstado(boolean estado);
	
	public List<Usuario> findByFundacion(Fundacion Fundacion);
	
	public List<Usuario> findByLocalidad(Localidad localidad);
	
	public Usuario findByCorreo(String correo);
	
	public List<Usuario> findByFundacionAndTipoUsuario(Fundacion Fundacion, String tipoUsuario);
	
	public List<Usuario> findByFundacionAndLocalidad(Fundacion Fundacion, Localidad localidad);
	
}
