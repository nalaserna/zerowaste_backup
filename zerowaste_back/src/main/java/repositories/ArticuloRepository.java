package repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import entities.Articulo;
import entities.Recogida;

public interface ArticuloRepository extends CrudRepository<Articulo, Long> {

	public Articulo findById(int id);
	
	public List<Articulo> findByRecogida(Recogida recogida);
	
	public List<Articulo> findByEstadoUtil(String estadoUtil);
	
	public List<Articulo> findByPeso(double peso);
	
	public List<Articulo> findByCantidad(int cantidad);
	
}
