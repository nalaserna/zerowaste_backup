package repositories;

import org.springframework.data.repository.CrudRepository;

import entities.Fundacion;
import entities.Localidad;

public interface FundacionRepository extends CrudRepository<Fundacion, Long> {

	public Fundacion findById(int id);
	
	public Fundacion findByNombre(String nombre);
	
	public Fundacion findByNit(String nit);
	
}
