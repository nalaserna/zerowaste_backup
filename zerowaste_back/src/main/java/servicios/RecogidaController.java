package servicios;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Articulo;
import entities.Fundacion;
import entities.Localidad;
import entities.Recogida;
import entities.Usuario;
import repositories.ArticuloRepository;
import repositories.FundacionRepository;
import repositories.LocalidadRepository;
import repositories.RecogidaRepository;
import repositories.UsuarioRepository;

@RestController
public class RecogidaController {
	
	@Autowired
	private RecogidaRepository recogidaRepositoryDAO;
	@Autowired
	private ArticuloRepository articuloRepositoryDAO;
	@Autowired
	private UsuarioRepository usuarioRepositoryDAO;
	@Autowired
	private LocalidadRepository localidadRepositoryDAO;
	@Autowired
	private FundacionRepository fundacionRepositoryDAO;
	
	@CrossOrigin
	@RequestMapping(path="/getAllRecogidas", method=RequestMethod.GET)
	public Iterable<Recogida> getAllRecogidas () {
		
		Iterable<Recogida> findAll = recogidaRepositoryDAO.findAll();
		
		return findAll;
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/addRecogida", method=RequestMethod.POST) 
	public @ResponseBody int addRecogida
	(@RequestParam String direccion, @RequestParam String id_localidad,
			@RequestParam String donador_id, @RequestParam String latitud,
			@RequestParam String longitud) {
		
		Localidad localidad = localidadRepositoryDAO.findById(Integer.parseInt(id_localidad));
		Usuario donador = usuarioRepositoryDAO.findById(Integer.parseInt(donador_id));
		Date hoy = Calendar.getInstance().getTime();
		System.out.println(direccion);
		Recogida nuevaRecogida = new Recogida();
	
		nuevaRecogida.setDireccion(direccion);
		nuevaRecogida.setLocalidad(localidad);
		nuevaRecogida.setFechaSolicitud(hoy);
		nuevaRecogida.setDonador(donador);
		nuevaRecogida.setLatitud(latitud);
		nuevaRecogida.setLongitud(longitud);
		recogidaRepositoryDAO.save(nuevaRecogida);
		System.out.println(nuevaRecogida.getId());
		return nuevaRecogida.getId();
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/updateRecogida", method=RequestMethod.POST) 
	public @ResponseBody String updateRecogida
	( @RequestParam String direccion, @RequestParam String id_localidad, @RequestParam String id) {
		
		Localidad localidad = localidadRepositoryDAO.findById(Integer.parseInt(id_localidad));
		
		Recogida recogida = recogidaRepositoryDAO.findById(Integer.parseInt(id));
		recogida.setLocalidad(localidad);
		recogida.setDireccion(direccion);
		recogidaRepositoryDAO.save(recogida);
		return "Recogida Actualizada";
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/asignarRecolectorRecogida", method=RequestMethod.POST) 
	public @ResponseBody String asignarRecolectorRecogida
	(@RequestParam String recolector_id, @RequestParam String id, 
			@RequestParam String fechaRecogidaEstimada) throws ParseException {
		
		Usuario recolector = usuarioRepositoryDAO.findById(Integer.parseInt(recolector_id));
		
		Recogida recogida = recogidaRepositoryDAO.findById(Integer.parseInt(id));
		
		Date fechaestimada = new SimpleDateFormat("yyyy-MM-dd").parse(fechaRecogidaEstimada); 
		
		recogida.setRecolector(recolector);
		recogida.setFechaEstimadaEntrega(fechaestimada);
		recogidaRepositoryDAO.save(recogida);
		return "Recolector asignado a la solicitud de recogida";
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/unassignRecolectorRecogida", method=RequestMethod.POST) 
	public @ResponseBody String unassignRecolectorRecogida
	(@RequestParam String id) {
		
		Recogida recogida = recogidaRepositoryDAO.findById(Integer.parseInt(id));
		recogida.setRecolector(null);
		recogida.setFechaEstimadaEntrega(null);
		recogidaRepositoryDAO.save(recogida);
		return "Recolector removido de la solicitud";
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/actualizarFechaRecogidaEstimada", method=RequestMethod.POST) 
	public @ResponseBody String actualizarFechaRecogidaEstimada
	(@RequestParam String id, @RequestParam String fechaRecogidaEstimada ) throws ParseException {
		
		Date fechaestimada = new SimpleDateFormat("yyyy-MM-dd").parse(fechaRecogidaEstimada); 
		
		Recogida recogida = recogidaRepositoryDAO.findById(Integer.parseInt(id));
		recogida.setFechaEstimadaEntrega(fechaestimada);
		
		recogidaRepositoryDAO.save(recogida);
		return "Fecha estimada de recogida actualizada";
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/completarRecogida", method=RequestMethod.POST) 
	public @ResponseBody String completarRecogida
	(@RequestParam String id) {
		Date hoy = Calendar.getInstance().getTime();
		
		Recogida recogida = recogidaRepositoryDAO.findById(Integer.parseInt(id));
		recogida.setFechaCompletada(hoy);
		recogidaRepositoryDAO.save(recogida);
		return "Recogida Completada";
	}
	
	@CrossOrigin
	@RequestMapping(path="/deleteRecogida", method=RequestMethod.POST) 
	public @ResponseBody String deleteRecogida
	(@RequestParam String id) {

		Recogida deleteRecogida = recogidaRepositoryDAO.findById(Integer.parseInt(id));
		recogidaRepositoryDAO.delete(deleteRecogida);
		return "Recogida Eliminada";	
	}
	
	@CrossOrigin
	@RequestMapping (path="/getRecogidaById", method=RequestMethod.POST)
	public Recogida getRecogidaById (@RequestParam String id) {
		
		return recogidaRepositoryDAO.findById(Integer.parseInt(id));
	}
	
	@CrossOrigin
	@RequestMapping (path="/getRecogidaByDonador", method=RequestMethod.POST)
	public Iterable<Recogida> getRecogidaByUser (@RequestParam String user_id) {
		
		Usuario user = usuarioRepositoryDAO.findById(Integer.parseInt(user_id));
		
		Iterable<Recogida> recogidas = recogidaRepositoryDAO.findByDonador(user);
		
		return recogidas;		
	}
	
	@CrossOrigin
	@RequestMapping (path="/getRecogidaByRecolector", method=RequestMethod.POST)
	public Iterable<Recogida> getRecogidaByRecolector (@RequestParam String user_id) {
		
		Usuario user = usuarioRepositoryDAO.findById(Integer.parseInt(user_id));
		
		Iterable<Recogida> recogidas = recogidaRepositoryDAO.findByRecolector(user);
		
		return recogidas;		
	}
	
	@CrossOrigin
	@RequestMapping (path="/getRecogidaByLocalidad", method=RequestMethod.POST)
	public Iterable<Recogida> getRecogidaByLocalidad (@RequestParam String nombre_localidad) {
		
		Localidad localidad = localidadRepositoryDAO.findByNombre(nombre_localidad);
		
		Iterable<Recogida> recogidas = recogidaRepositoryDAO.findByLocalidad(localidad);
		
		return recogidas;		
	}
	
	@CrossOrigin
	@RequestMapping (path="/getRecogidasPendientes", method=RequestMethod.GET)
	public Iterable<Recogida> getRecogidasPendientes () {
		
		Iterable<Recogida> recogidas = recogidaRepositoryDAO.findByFechaEstimadaEntrega(null);
		
		return recogidas;		
	}
	
	
	
	@CrossOrigin
	@RequestMapping (path="/RecogidasPendientes", method=RequestMethod.POST)
	public Iterable<Recogida> getRecogidaByStatus () {
	
		
		Iterable<Recogida> recogidas = recogidaRepositoryDAO.findByFechaCompletada(null);
		
		return recogidas;		
	}
	
	@CrossOrigin
	@RequestMapping (path="/getRecogidaByFecha", method=RequestMethod.POST)
	public Iterable<Recogida> RecogidasAsignadas (String fecha) throws ParseException {
	
		Date f = new SimpleDateFormat("yyyy-MM-dd").parse(fecha); 
		
		Iterable<Recogida> recogidas = recogidaRepositoryDAO.findByFechaSolicitud(f);
		
		return recogidas;		
	}
	
	@CrossOrigin
	@RequestMapping (path="/getRecogidaByFechaDonadorRecolector", method=RequestMethod.POST)
	public Iterable<Recogida> RecogidasAsignadas (String fecha, String donador_id, String recolector_id) throws ParseException {
	
		Date f = new SimpleDateFormat("yyyy-MM-dd").parse(fecha); 
		Usuario recolector = usuarioRepositoryDAO.findById(Integer.parseInt(recolector_id));
		Usuario donador = usuarioRepositoryDAO.findById(Integer.parseInt(donador_id));
		
		Iterable<Recogida> recogidas = recogidaRepositoryDAO.findByFechaEstimadaEntregaAndRecolectorAndDonador(f, recolector, donador);
		
		return recogidas;		
	}
}
