package servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Articulo;
import entities.Recogida;
import entities.Usuario;
import repositories.ArticuloRepository;
import repositories.RecogidaRepository;
import repositories.UsuarioRepository;

@RestController
public class ArticuloController {
	
	@Autowired
	private ArticuloRepository articuloRepositoryDAO;
	@Autowired
	private RecogidaRepository recogidaRepositoryDAO;
	
	@CrossOrigin
	@RequestMapping("/getAllArticulos")
	public Iterable<Articulo> getAllArticulos () {
		
		Iterable<Articulo> findAll = articuloRepositoryDAO.findAll();
		return findAll;
		
	}
	
	@CrossOrigin
	@RequestMapping("/getAllArticulosRecogida")
	public Iterable<Articulo> getAllArticulosRecogida (@RequestParam String id_Recogida) {
		Recogida recogida = recogidaRepositoryDAO.findById(Integer.parseInt(id_Recogida));
		
		Iterable<Articulo> findAll = articuloRepositoryDAO.findByRecogida(recogida);
		return findAll;
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/addArticulo", method=RequestMethod.POST) 
	public @ResponseBody String addNewArticulo
	(@RequestParam String id_Recogida, @RequestParam String nombre,
			@RequestParam String cantidad, @RequestParam String estadoUtil,
			@RequestParam String alto, @RequestParam String ancho, 
			@RequestParam String largo, @RequestParam String peso, 
			@RequestParam String descripcion, @RequestParam String imagen1,
			@RequestParam String imagen2) {
		
		Recogida recogida = recogidaRepositoryDAO.findById(Integer.parseInt(id_Recogida));
		
		Articulo nuevoArticulo = new Articulo();
		nuevoArticulo.setRecogida(recogida);
		nuevoArticulo.setNombre(nombre);
		nuevoArticulo.setPeso(Integer.parseInt(peso));
		nuevoArticulo.setEstadoUtil(estadoUtil);
		nuevoArticulo.setCantidad(Integer.parseInt(cantidad));
		nuevoArticulo.setAlto(Double.parseDouble(alto));
		nuevoArticulo.setAncho(Double.parseDouble(ancho));
		nuevoArticulo.setDescripcion(descripcion);
		nuevoArticulo.setImagen1(imagen1);
		nuevoArticulo.setImagen2(imagen2);
		nuevoArticulo.setLargo(Double.parseDouble(largo));
		articuloRepositoryDAO.save(nuevoArticulo);
		return "Articulo Guardado";
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/deleteArticulo", method=RequestMethod.POST) 
	public @ResponseBody String deleteArticulo
	(@RequestParam String id) {
		
		Articulo deleteArticulo = articuloRepositoryDAO.findById(Integer.parseInt(id));
		articuloRepositoryDAO.delete(deleteArticulo);
		return "Articulo Eliminado";
		
	}
	

	@CrossOrigin
	@RequestMapping ("/getArticuloEstado")
	public Iterable<Articulo> getArticuloByEstado (@RequestParam String estado) {
		
		Iterable<Articulo> articulos = articuloRepositoryDAO.findByEstadoUtil(estado);
		
		return articulos;
	}
	
	@CrossOrigin
	@RequestMapping ("/getArticuloByPeso")
	public Iterable<Articulo> getArticuloByPeso (@RequestParam double peso) {
		
		Iterable<Articulo> articulos = articuloRepositoryDAO.findByPeso(peso);
		
		return articulos;
	}
	
	@CrossOrigin
	@RequestMapping ("/getArticuloByCantidad")
	public Iterable<Articulo> getArticuloByCantidad (@RequestParam int cantidad) {
		
		Iterable<Articulo> articulos = articuloRepositoryDAO.findByCantidad(cantidad);
		
		return articulos;
	}
	
	@CrossOrigin
	@RequestMapping ("/getArticuloById")
	public Articulo getArticuloById (@RequestParam String id) {
		
		return articuloRepositoryDAO.findById(Integer.parseInt(id));
		
	}
}
