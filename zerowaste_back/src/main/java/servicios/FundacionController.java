package servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Fundacion;
import entities.Localidad;
import repositories.FundacionRepository;


@RestController
public class FundacionController {

	@Autowired
	private FundacionRepository fundacionRepositoryDAO;
	
	@CrossOrigin
	@RequestMapping("/getAllFundaciones")
	public Iterable<Fundacion> getAllFundaciones () {
		
		Iterable<Fundacion> findAll = fundacionRepositoryDAO.findAll();
		
		return findAll;
	}
	
	@CrossOrigin
	@RequestMapping(path="/addFundacion", method=RequestMethod.POST) 
	public @ResponseBody String addFundacion
	(@RequestParam String nombre, @RequestParam String direccion, 
			@RequestParam String nit) {
		
		Fundacion nuevaFundacion = new Fundacion();
		nuevaFundacion.setNombre(nombre);
		nuevaFundacion.setDireccion(direccion);
		nuevaFundacion.setNit(nit);
		fundacionRepositoryDAO.save(nuevaFundacion);
		return "Fundación Guardada";
	}
	
	@CrossOrigin
	@RequestMapping(path="/deleteFundacion", method=RequestMethod.POST) 
	public @ResponseBody String deleteFundacion
	(@RequestParam String id) {
		
		Fundacion deleteFundacion = fundacionRepositoryDAO.findById(Integer.parseInt(id));
		fundacionRepositoryDAO.delete(deleteFundacion);
		return "Fundacion Eliminada";
		
	}
	
	@CrossOrigin
	@RequestMapping ("/getFundacionByNombre")
	public Fundacion getFundacionByNombre (@RequestParam String nombre) {
		
		return fundacionRepositoryDAO.findByNombre(nombre);
		
	}
	
	@CrossOrigin
	@RequestMapping ("/getFundaciondByNit")
	public Fundacion getFundaciondByNit (@RequestParam String nit) {
		
		return fundacionRepositoryDAO.findByNit(nit);
		
	}
	
	@CrossOrigin
	@RequestMapping ("/getFundaciondById")
	public Fundacion getFundaciondById (@RequestParam String id) {
		
		return fundacionRepositoryDAO.findById(Integer.parseInt(id));
		
	}
}
