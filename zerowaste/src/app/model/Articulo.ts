import { Recogida } from './Recogida';

export class Articulo{

   public id: number;
   public nombre: string;
   public cantidad: number;
   public descripcion: string;
   public estadoUtil: string;
   public imagen1: string;
   public imagen2: string;
   public alto: number;
   public ancho: number;
   public largo: number;
   public peso: number;
   public recogida: Recogida;

}
