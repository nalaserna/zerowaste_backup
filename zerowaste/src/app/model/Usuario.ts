
export class Usuario {

  public id: number;
  public nombre: string;
  public apellido: string;
  public password: string;
  public correo: string;
  public cedula: string;
  public estado: boolean;
  public fechaNacimiento: Date;
  public tipoUsuario: string;
  public fundacion: Number;
  public localidad: Number;
  public fechaCreacion: Date;
  public celular: number;

}
