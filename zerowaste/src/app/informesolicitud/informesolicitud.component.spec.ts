import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformesolicitudComponent } from './informesolicitud.component';

describe('InformesolicitudComponent', () => {
  let component: InformesolicitudComponent;
  let fixture: ComponentFixture<InformesolicitudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformesolicitudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformesolicitudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
