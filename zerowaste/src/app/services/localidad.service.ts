import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Localidad } from '../model/Localidad';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalidadService {

  constructor(private http: HttpClient) { }

  public getAllLocalidades(): Observable<Localidad[]> {
    return this.http.get<Localidad[]>(environment.urlgetAllLocalidades);
  }

  public getLocalidadById(id: number): Observable<Localidad> {
    const body = new HttpParams().set('id', id+'');
    return this.http.post<Localidad>(environment.urlgetLocalidadById, body);
  }

  public getLocalidadByNombre(nombre: string): Observable<Localidad> {
    const body = new HttpParams().set('nombre', nombre+'');
    return this.http.post<Localidad>(environment.urlgetLocalidadByNombre, body);
  }

}