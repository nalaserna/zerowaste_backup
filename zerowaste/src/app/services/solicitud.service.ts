import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Recogida } from '../model/Recogida';


@Injectable({
  providedIn: 'root'
})
export class SolicitudService {
  //  [x: string]: any;

  constructor(private http: HttpClient) { }

  public getAllSolicitudes(): Observable<Recogida[]> {
    return this.http.get<Recogida[]>(environment.urlConsultarAllRecogidas);
  }

  public addRecogida(direccion: string, localidad: Number, donador_id: string, latitud: Number, longitud: Number): Observable<string> {

    const parametros = new HttpParams()
      .set('direccion', direccion)
      .set('id_localidad', localidad.toString())
      .set('donador_id', donador_id.toString())
      .set('latitud', latitud.toString())
      .set('longitud', longitud.toString());
    let httpParams = parametros;

    return this.http.post<string>(environment.urladdRecogida, httpParams);
  }

  public updateRecogida(direccion: string, id_localidad: number, id: number): Observable<string> {

    const parametros = new HttpParams()
      .set('direccion', direccion)
      .set('id_localidad', id_localidad.toString())
      .set('id', id.toString());
    let httpParams = parametros;

    return this.http.post<string>(environment.urlupdateRecogida, httpParams);
  }

  public getSolicitudesPendientes(): Observable<Recogida[]> {
    return this.http.get<Recogida[]>(environment.urlConsultaRecogidasPendientes);
  }

  public getSolicitudesByFechaCreacion(fecha: Date): Observable<Recogida[]> {
    const parametros = new HttpParams()
      .set('fecha', fecha.toString());
      let httpParams = parametros;
    return this.http.post<Recogida[]>(environment.urlRecogidasByFechaCreacion, httpParams);
  }

  public getRecogidaByFechaDonadorRecolector(fecha: Date, donador: Number, recolector: Number): Observable<Recogida[]> {
    const parametros = new HttpParams()
      .set('fecha', fecha.toString())
      .set('donador_id', donador.toString())
      .set('recolector_id', recolector.toString());
      let httpParams = parametros;
    return this.http.post<Recogida[]>(environment.urlRecogidasByFechaDonadorRecolector, httpParams);
  }

  public asignarRecolectorRecogida(recolector_id: number, id: number, fechaRecogidaEstimada: Date): Observable<string> {

    const parametros = new HttpParams()
      .set('recolector_id', recolector_id.toString())
      .set('id', id.toString())
      .set('fechaRecogidaEstimada', fechaRecogidaEstimada.toString());
    let httpParams = parametros;


    return this.http.post<string>(environment.urlasignarRecolectorRecogida, httpParams);
  }

  public actualizarFechaRecogidaEstimada(id: number, fechaRecogidaEstimada: Date): Observable<string> {

    const parametros = new HttpParams()
      .set('id', id.toString())
      .set('fechaRecogidaEstimada', fechaRecogidaEstimada.toString());
    let httpParams = parametros;

    return this.http.post<string>(environment.urlactualizarFechaRecogidaEstimada, httpParams);
  }

  public completarRecogida(id: string): Observable<string> {
    const body = new HttpParams().set('id', id+'');
    return this.http.post<string>(environment.urlcompletarRecogida, body);
  }

  public deleteRecogida(id: string): Observable<string> {
    const body = new HttpParams().set('id', id+'');
    return this.http.post<string>(environment.urldeleteRecogida, body);
  }

  public getRecogidaById(id: string): Observable<Recogida> {
    const body = new HttpParams().set('id', id+'');
    return this.http.post<Recogida>(environment.urlgetRecogidaById, body);
  }

  public getRecogidasByDonador(userid: string): Observable<Recogida[]>{
    const body = new HttpParams().set('user_id', userid+'');
    return this.http.post<Recogida[]>(environment.urlgetRecogidaByDonador, body);
  }

  public getRecogidasByRecolector(userid: string): Observable<Recogida[]>{
    const body = new HttpParams().set('user_id', userid+'');
    return this.http.post<Recogida[]>(environment.urlgetRecogidaByRecolector, body);
  }

  public getRecogidasByLocalidad(nombre_localidad: string): Observable<Recogida[]>{
    const body = new HttpParams().set('nombre_localidad', nombre_localidad+'');
    return this.http.post<Recogida[]>(environment.urlgetRecogidaByLocalidad, body);
  }


}