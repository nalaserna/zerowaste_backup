import { Component, OnInit, Input } from '@angular/core';
import { Localidad } from '../model/Localidad';
import { Usuario } from '../model/Usuario';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';
import { LocalidadService } from '../services/localidad.service';
import { FundacionService } from '../services/fundacion.service';
import { Fundacion } from '../model/Fundacion';

@Component({
  selector: 'app-newrecolector',
  templateUrl: './newrecolector.component.html',
  styleUrls: ['./newrecolector.component.css']
})
export class NewrecolectorComponent implements OnInit {

  fundaciones: Array<Fundacion>;
  localidades: Array<Localidad>;
  newRecolector: Usuario;
  formularioadicionperfilrecolector: FormGroup;

  constructor(private router: Router, private fundacionService: FundacionService, private localidadService: LocalidadService, private usuarioService: UsuarioService) {
    this.newRecolector = new Usuario();

  }


  ngOnInit() {
    this.localidadService.getAllLocalidades().subscribe(resp => {
      this.localidades = resp;
      console.log(this.localidades);
    });

    this.fundacionService.getAllFundaciones().subscribe(resp => {
      this.fundaciones = resp;
      console.log(this.fundaciones);
    });

  }

  saveusuario(nombre: string, apellido: string, fechaNacimiento: Date, fundacion: Number, localidad: Number) {
    console.log(fundacion)
    this.usuarioService.addNewRecolector(nombre,
      fechaNacimiento.toString(), fundacion,
      localidad, apellido).subscribe();
    alert("Recolector creado")
    location.reload();
  }

}
