import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ZeroWaste';
  activo: string;
  rol: string;
  

  constructor(private router: Router, private route: ActivatedRoute) {
    this.activo = this.route.snapshot.params['id'];

    this.rol = this.route.snapshot.params['rol'];

   this.activo= window.localStorage.getItem('idUsuario');
    this.rol= window.localStorage.getItem('rol');
  }



  ngOnInit() {

    // window.localStorage.removeItem('rol');
    //window.localStorage.removeItem('idUsuario');
    //  window.localStorage.setItem('rol','fundacion');

    window.localStorage.setItem('idUsuario', this.activo);
   window.localStorage.setItem('rol', this.rol);

    console.log(this.activo);

   /* if (this.activo == null) {
      window.location.href = 'http://localhost:4201/login';
    }*/
  }


}


