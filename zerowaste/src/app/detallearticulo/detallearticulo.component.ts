import { Component, OnInit, Input } from '@angular/core';
import { Articulo } from '../model/Articulo';
import { ActivatedRoute, Router } from '@angular/router';
import { SolicitudService } from '../services/solicitud.service';
import { ArticuloService } from '../services/articulo.service';

@Component({
  selector: 'app-detallearticulo',
  templateUrl: './detallearticulo.component.html',
  styleUrls: ['./detallearticulo.component.css']
})
export class DetallearticuloComponent implements OnInit {

  @Input() mySelectedArticulo: Articulo;
  @Input() misArticulos: Array<Articulo>;
  @Input() recogidaid: number;

  constructor(private route: ActivatedRoute, private articuloService: ArticuloService, private router: Router) {
    this.recogidaid = this.route.snapshot.params['id'];
  }

  ngOnInit() {
  }

  public deleteArticulo(idarticulo: number) {
    this.articuloService.deleteArticulo(idarticulo).subscribe();
    alert("Artículo eliminado")
    location.reload();
  }

}
