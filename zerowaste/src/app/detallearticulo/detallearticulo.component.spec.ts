import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallearticuloComponent } from './detallearticulo.component';

describe('DetallearticuloComponent', () => {
  let component: DetallearticuloComponent;
  let fixture: ComponentFixture<DetallearticuloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallearticuloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallearticuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
