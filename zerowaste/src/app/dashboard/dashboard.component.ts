import { Component, OnInit, Input } from '@angular/core';
import { Recogida } from '../model/Recogida';
import { Usuario } from '../model/Usuario';
import { SolicitudService } from '../services/solicitud.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  misRecogidas: Array<Recogida>;
  miRecogida: Recogida;
  user: Usuario;
  rol: string;
  activo: string;

  constructor(private solicitudService: SolicitudService, private router: Router, private route: ActivatedRoute) {
    this.rol = '';
    this.miRecogida = new Recogida();
    this.user = new Usuario();

    
    
    console.log(this.activo);
    console.log(this.rol);

    

  }


  ngOnInit() {
    this.activo = this.route.snapshot.params['id'];

    this.rol = this.route.snapshot.params['rol'];
    window.localStorage.setItem('rol', this.rol);
    window.localStorage.setItem('idUsuario', this.activo);

    if (this.activo == null) {
      this.router.navigateByUrl('/login');
    }

    if(this.rol !='Donador'){
      this.solicitudService.getAllSolicitudes().subscribe(resp => {
        this.misRecogidas = resp;
        console.log('Solicitudes consultadas');
      });
      
    }else if(this.rol =='Donador'){
      this.solicitudService.getRecogidasByDonador(window.localStorage.getItem('idUsuario')).subscribe(resp =>{
        this.misRecogidas=resp;
        
        console.log('Solicitudes consultadas para id: '+this.activo);
      });
     /* if (this.misRecogidas == null){
          alert('No has creado ninguna solicitud hasta el momento. ¡Anímate a donar!');
          this.router.navigateByUrl('/newsolicitud');
      }*/
    }

  }

  verDetalle(recogida: number) {
    this.router.navigate(['/versolicitud', recogida]);
  }

}
