import { Component, OnInit, Input, ViewChild, NgZone, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalidadService } from '../services/localidad.service';
import { Localidad } from '../model/Localidad';
import { Recogida } from '../model/Recogida';
import { SolicitudService } from '../services/solicitud.service';
import { MapsAPILoader, MouseEvent, AgmCoreModule } from '@agm/core';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-addsolicitud',
  templateUrl: './addsolicitud.component.html',
  styleUrls: ['./addsolicitud.component.css']
})
export class AddsolicitudComponent implements OnInit {

  localidades: Array<Localidad>;
  miSolicitud: Recogida;
  activo : string;
  idrecogida: string;
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder;

  @ViewChild('search')
  public searchElementRef: ElementRef;



  constructor(private router: Router, private route: ActivatedRoute, private localidadService: LocalidadService, private solicitudService: SolicitudService,
    private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {
    this.miSolicitud = new Recogida();

  }

  ngOnInit() {

    this.activo = window.localStorage.getItem('idUsuario')
    console.log(this.activo);

    if (this.activo == null) {
      this.router.navigateByUrl('/login');
    }


    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 18;
        });
      });
    });

    this.localidadService.getAllLocalidades().subscribe(resp => {
      this.localidades = resp;
      console.log(this.localidades);
    });

  }


  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 18;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }


  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 18;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }

  crearSolicitud() {
    console.log(this.latitude);
    console.log(this.longitude);
    console.log(this.address);
    console.log(this.miSolicitud.direccion);
    this.miSolicitud.latitud = this.latitude+'';
    this.miSolicitud.longitud = this.longitude+'';
    console.log('Latitud guardada '  +this.miSolicitud.latitud);
    console.log('Longitud guardada ' +this.miSolicitud.longitud);

    if (this.miSolicitud.direccion == null){
      this.miSolicitud.direccion = this.address;
      console.log(this.miSolicitud.direccion);
    }

    console.log('Solicitud:', this.miSolicitud.localidad);
    this.solicitudService.addRecogida(this.miSolicitud.direccion, this.miSolicitud.localidad, window.localStorage.getItem('idUsuario'), this.latitude, this.longitude).subscribe(
      resp => {
        this.idrecogida = resp;
        console.log(this.idrecogida);
        alert("Solicitud de recogida creada");
        this.router.navigate(['/addarticulo', this.idrecogida]);
      }
    );
    //
    

  }
}
