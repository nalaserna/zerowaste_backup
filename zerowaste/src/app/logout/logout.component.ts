import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.localStorage.removeItem('idUsuario');
    window.localStorage.removeItem('rol');
    window.location.href = 'http://localhost:4201/login';
  }

}
