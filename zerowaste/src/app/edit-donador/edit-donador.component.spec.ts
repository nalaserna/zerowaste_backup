import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDonadorComponent } from './edit-donador.component';

describe('EditDonadorComponent', () => {
  let component: EditDonadorComponent;
  let fixture: ComponentFixture<EditDonadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDonadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDonadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
