import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddsolicitudComponent } from './addsolicitud/addsolicitud.component';
import { ModperfilComponent } from './modperfil/modperfil.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DetallesolicitudComponent } from './detallesolicitud/detallesolicitud.component';
import { VersolicitudComponent } from './versolicitud/versolicitud.component';
import { InformesolicitudComponent } from './informesolicitud/informesolicitud.component';
import { InformeusuariosComponent } from './informeusuarios/informeusuarios.component';
import { PerfilrecolectorComponent } from './perfilrecolector/perfilrecolector.component';
import { DetalledashboardComponent } from './detalledashboard/detalledashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { DetallearticuloComponent } from './detallearticulo/detallearticulo.component';
import { FotosArticulosComponent } from './fotos-articulos/fotos-articulos.component';
import { NewrecolectorComponent } from './newrecolector/newrecolector.component';
import { FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { EditDonadorComponent } from './edit-donador/edit-donador.component';
import { AddarticuloComponent } from './addarticulo/addarticulo.component';
import { InformeRecolectorComponent } from './informe-recolector/informe-recolector.component';
import { SolicitudespendientesComponent } from './solicitudespendientes/solicitudespendientes.component';
import { VerrecolectoresComponent } from './verrecolectores/verrecolectores.component';
import { AgmCoreModule } from '@agm/core';
import { RutaMapsComponent } from './ruta-maps/ruta-maps.component';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  declarations: [
    AppComponent,
    AddsolicitudComponent,
    ModperfilComponent,
    DashboardComponent,
    DetallesolicitudComponent,
    VersolicitudComponent,
    InformesolicitudComponent,
    InformeusuariosComponent,
    PerfilrecolectorComponent,
    DetalledashboardComponent,
    DetallearticuloComponent,
    FotosArticulosComponent,
    NewrecolectorComponent,
    AddarticuloComponent,
    EditDonadorComponent,
    InformeRecolectorComponent,
    SolicitudespendientesComponent,
    VerrecolectoresComponent,
    RutaMapsComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: ' AIzaSyCjx2dBcbPnCJc1Rc0yXMmognoV4G-BxGo ',
      libraries: ['places']
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
