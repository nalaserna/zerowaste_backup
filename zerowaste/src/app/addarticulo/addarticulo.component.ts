import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SolicitudService } from '../services/solicitud.service';
import { LocalidadService } from '../services/localidad.service';
import { Articulo } from '../model/Articulo';
import { ArticuloService } from '../services/articulo.service';

@Component({
  selector: 'app-addarticulo',
  templateUrl: './addarticulo.component.html',
  styleUrls: ['./addarticulo.component.css']
})
export class AddarticuloComponent implements OnInit {

  @Input() recogidaid : number;
  miArticulo: Articulo;

  constructor(private router: Router, private route: ActivatedRoute,private articuloService: ArticuloService) {
    this.recogidaid = this.route.snapshot.params['id'];
    this.miArticulo = new Articulo();
   }

  ngOnInit() {
  }

  public addArticulo(nombre: string, cantidad:number, estadoUtil:string, alto:number,
    ancho:number, largo:number, peso:number, descripcion:string, imagen1:string, imagen2: string ){
      console.log(descripcion);
    this.articuloService.addNewArticulo(this.recogidaid, nombre, cantidad,
      estadoUtil, alto, ancho, largo,
      peso,descripcion, imagen1,imagen2).subscribe();

      this.router.navigate(['/detallesolicitud', this.recogidaid]);
  }
}
