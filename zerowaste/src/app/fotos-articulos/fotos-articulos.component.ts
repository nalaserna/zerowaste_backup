import { Component, OnInit, Input } from '@angular/core';
import { Articulo } from '../model/Articulo';
import { ActivatedRoute } from '@angular/router';
import { Recogida } from '../model/Recogida';
import { SolicitudService } from '../services/solicitud.service';
import { ArticuloService } from '../services/articulo.service';

@Component({
  selector: 'app-fotos-articulos',
  templateUrl: './fotos-articulos.component.html',
  styleUrls: ['./fotos-articulos.component.css']
})
export class FotosArticulosComponent implements OnInit {

  @Input() mySelectedRecogida: Recogida;

  @Input() recogidaid: number;
  misArticulos: Articulo[];

  constructor(private route: ActivatedRoute, private api: ArticuloService) {
    this.recogidaid = this.route.snapshot.params['id'];




  }

  ngOnInit() {
    this.api.getAllArticulosRecogida(this.recogidaid.toString()).subscribe(
      arti => this.misArticulos = arti
    ); // si no se hace asi se queja por el observable
  }

}
