import { Component, OnInit } from '@angular/core';
import { Usuario } from '../model/Usuario';
import { UsuarioService } from '../services/usuario.service';
import { Router } from '@angular/router';
import { Fundacion } from '../model/Fundacion';
import { Localidad } from '../model/Localidad';

@Component({
  selector: 'app-informe-recolector',
  templateUrl: './informe-recolector.component.html',
  styleUrls: ['./informe-recolector.component.css']
})
export class InformeRecolectorComponent implements OnInit {

  misRecolectores: Array<Usuario>;
  miRecolector: Usuario;
  user: Usuario;
  idlocalidad: Number;
  idfundacion: Number;
  fundaciones: Array<Fundacion>;
  localidades: Array<Localidad>;

  constructor(private usuarioService: UsuarioService, private router: Router) {
    this.miRecolector = new Usuario();
    this.user = new Usuario();
    usuarioService.getAllRecolectores().subscribe(resp => {
      this.misRecolectores = resp;
      console.log('Todos los recolectores');
    });
   }

  ngOnInit() {
  }

  public verPerfil(idrecolector: number){
    this.router.navigate(['/perfilrecolector', idrecolector]);
  }

  public enlistarRecolectores() {
    this.usuarioService.getAllRecolectoresFundacionLocalidad(this.idlocalidad, this.idfundacion).subscribe(resp => {
      this.misRecolectores = resp;
      console.log('Todos los recolectores');
    });

    location.reload();

  }

}
