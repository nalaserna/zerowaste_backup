import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalledashboardComponent } from './detalledashboard.component';

describe('DetalledashboardComponent', () => {
  let component: DetalledashboardComponent;
  let fixture: ComponentFixture<DetalledashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalledashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalledashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
