import { Component, OnInit, Input } from '@angular/core';
import { Recogida } from '../model/Recogida';
import { Usuario } from '../model/Usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detalledashboard',
  templateUrl: './detalledashboard.component.html',
  styleUrls: ['./detalledashboard.component.css']
})
export class DetalledashboardComponent implements OnInit {

  constructor(private router:Router) { }

  @Input() MySelectedRecogida: Recogida;
  @Input() usuario: Usuario;

  ngOnInit() {
  }
}
