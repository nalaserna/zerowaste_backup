import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerrecolectoresComponent } from './verrecolectores.component';

describe('VerrecolectoresComponent', () => {
  let component: VerrecolectoresComponent;
  let fixture: ComponentFixture<VerrecolectoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerrecolectoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerrecolectoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
