import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsuarioService } from '../services/usuario.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ruta-maps',
  templateUrl: './ruta-maps.component.html',
  styleUrls: ['./ruta-maps.component.css']
})
export class RutaMapsComponent implements OnInit {

  locacion : any[] = [];
  lat : number;
  lng : number;
  constructor(private http: HttpClient, private servicio: UsuarioService, private router: ActivatedRoute) { 

   }
  ngOnInit() {
    this.lat = this.router.snapshot.params['lat'];
    this.lng = this.router.snapshot.params['lng'];
    window.location.href = 'https://www.google.com/maps/dir/?api=1&destination='+this.lat+','+this.lng;

  }

}
