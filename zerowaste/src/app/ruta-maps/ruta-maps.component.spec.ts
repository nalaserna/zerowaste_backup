import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaMapsComponent } from './ruta-maps.component';

describe('RutaMapsComponent', () => {
  let component: RutaMapsComponent;
  let fixture: ComponentFixture<RutaMapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaMapsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
