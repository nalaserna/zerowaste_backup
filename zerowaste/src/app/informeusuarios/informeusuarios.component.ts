import { Component, OnInit, Input } from '@angular/core';
import { Localidad } from '../model/Localidad';
import { LocalidadService } from '../services/localidad.service';
import { UsuarioService } from '../services/usuario.service';
import { Usuario } from '../model/Usuario';
import { Fundacion } from '../model/Fundacion';
import { FundacionService } from '../services/fundacion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-informeusuarios',
  templateUrl: './informeusuarios.component.html',
  styleUrls: ['./informeusuarios.component.css']
})
export class InformeusuariosComponent implements OnInit {

  localidades: Array<Localidad>;
  @Input() recolectores: Array<Usuario>;
  usuarios: Array<Usuario>;
  fundaciones: Array<Fundacion>;
  idfundacion: string;
  usuariosxFundacion: Array<Usuario>;

  recolectorConsulta: Usuario;
  recolectorEliminado: string;

  @Input() visible: number;

  constructor(private localidadService: LocalidadService, private recolectorService: UsuarioService, private fundacionService: FundacionService, private router: Router) { }

  ngOnInit() {

    this.idfundacion = window.localStorage.getItem('idUsuario');

    this.localidadService.getAllLocalidades().subscribe(resp => {
      this.localidades = resp;
      console.log(this.localidades);
    });

    this.fundacionService.getAllFundaciones().subscribe(resp => {
      console.log('usuarios ');
      this.fundaciones = resp;
    });

    this.consultarUsuarioxFundacion();
    

    // this.recolectorService.getAllRecolectores().subscribe(resp => {
    //   console.log('Recolectores ');
    //   this.recolectores = resp;
    // });

    // this.recolectorService.getAllUsers().subscribe(resp => {
    //   console.log('usuarios ');
    //   this.usuarios = resp;
    // });

  }

  consultarUsuarioxFundacion() {
   
    this.recolectorService.getAllRecolectoresByFundacion(this.idfundacion).subscribe(resp => {
      console.log('usuarios ');
      this.recolectores = resp;
    });
  }

  eliminarRecolector(idRecolector: number) {
    this.recolectorService.deleteRecolector(idRecolector).subscribe(resp => {
      console.log('usuarios ');
      this.recolectorEliminado = resp;
    });
  }

  verPerfilRecolector(idrecolector: number) {
    this.router.navigate(['/perfilrecolector', idrecolector]);
  }


}
