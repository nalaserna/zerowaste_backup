import { VersolicitudComponent } from './versolicitud/versolicitud.component';
import { DetallesolicitudComponent } from './detallesolicitud/detallesolicitud.component';
import { ModperfilComponent } from './modperfil/modperfil.component';
import { AddsolicitudComponent } from './addsolicitud/addsolicitud.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InformeusuariosComponent } from './informeusuarios/informeusuarios.component';
import { InformesolicitudComponent } from './informesolicitud/informesolicitud.component';
import { PerfilrecolectorComponent } from './perfilrecolector/perfilrecolector.component';
import { NewrecolectorComponent } from './newrecolector/newrecolector.component';
import { AddarticuloComponent } from './addarticulo/addarticulo.component';
import { SolicitudespendientesComponent } from './solicitudespendientes/solicitudespendientes.component';
import { InformeRecolectorComponent } from './informe-recolector/informe-recolector.component';
import { RutaMapsComponent } from './ruta-maps/ruta-maps.component';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [

  { path: 'newsolicitud', component: AddsolicitudComponent },
  { path: 'modperfil/:id', component: ModperfilComponent },
  { path: 'dashboard/:id/:rol', component: DashboardComponent },
  { path: 'detallesolicitud/:id', component: DetallesolicitudComponent },
  { path: 'versolicitud/:id', component: VersolicitudComponent },
  { path: 'informeusuarios', component: InformeRecolectorComponent },
  { path: 'informesolicitud', component: InformesolicitudComponent },
  { path: 'perfilrecolector/:id', component: PerfilrecolectorComponent },
  { path: 'addRecolector', component: NewrecolectorComponent },
  { path: 'addarticulo/:id', component: AddarticuloComponent},
  {path: 'locacion/:lat/:lng', component: RutaMapsComponent},
  { path: 'solicitudespendientes', component: SolicitudespendientesComponent},
  { path: 'logout', component: LogoutComponent},


  { path: '**', pathMatch: 'full', redirectTo: 'dashboard/:id/:rol' }

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
